<?php
class database_handler {
	function __construct() {
		$this->username = 'root';
		$this->password = '';
		$this->host = 'localhost';
		$this->name = 'marso';
		try {
			$this->conn = new PDO ( "mysql:host=" . db_HOST . ";dbname=" . db_NAME . ";charset=utf8", db_USERNAME, db_PASSWORD );
			$this->conn->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $e ) {
			// echo "Error: " . $e->getMessage();
		}
	}
	public function action_query($str, $param) {
		try {
			$state = $this->conn->prepare ( $str );
			$state->execute ( $param );
			return true;
		} catch ( PDOException $e ) {
			echo "Error: " . $e->getMessage ();
			return false;
		}
	}
	public function select_query($str, $param) {
		try {
			$statement = $this->conn->prepare ( $str );
			$statement->execute ( $param );
			$result = $statement->fetchAll ( PDO::FETCH_ASSOC );
			if (sizeof ( $result ) == 0)
				return false;
			else
				return $result;
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
			return false;
		}
	}
}

?>