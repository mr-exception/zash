<?php
// all statics
include_once 'includes/config.php';
include_once 'includes/database_handler.php';
$db = new database_handler ();

define ( 'INVALID_COUNT', 'E1' );

$count = $_GET ['count'];

if (! is_numeric ( $count ))
	die ( INVALID_COUNT );
if ($count < 0)
	die ( INVALID_COUNT );
	
	// search for username of phone_Id in database
$search_result = $db->select_query ( 'SELECT `username` FROM `' . TABLE_PREFIX . 'users` ORDER BY score DESC LIMIT ' . $count, array (
		$count 
) );

if ($search_result) {
	echo json_encode ( $search_result );
} else {
	echo USERNAME_NOT_FOUND;
}
