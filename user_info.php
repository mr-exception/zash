<?php
// all statics
include_once 'includes/config.php';
include_once 'includes/database_handler.php';
$db = new database_handler ();

define ( 'USERNAME_NOT_FOUND', 'E1' );

$username = $_GET ['username'];

// search for username of phone_Id in database
$search_result = $db->select_query ( 'SELECT * FROM ' . TABLE_PREFIX . 'users WHERE username = ?;', array (
		$username 
) );

if ($search_result) {
	$output = array (
			'username' => $search_result [0] ['username'],
			'score' => $search_result [0] ['score'],
			'avatar' => $search_result [0] ['avatar'] 
	);
	echo json_encode ( $output );
} else {
	echo USERNAME_NOT_FOUND;
}
