<?php
// all statics
include_once 'includes/config.php';
include_once 'includes/database_handler.php';
$db = new database_handler ();

define ( 'DONE', 'D0' );

define ( 'SHORT_USERNAME', 'E1' );
define ( 'INVALID_PHONE_ID', 'E2' );
define ( 'LONG_USERNAME', 'E5' );
define ( 'WRONG_AVATAR', 'E6' );

define ( 'USER_EXISTS', 'E3' );
define ( 'PHONE_ID_EXISTS', 'E4' );

$username = $_GET ['username'];
$phone_Id = $_GET ['phone_Id'];
$avatar = $_GET ['avatar'];

if (strlen ( $username ) < 6)
	die ( SHORT_USERNAME );
if (strlen ( $username ) > 20)
	die ( LONG_USERNAME );
if (strlen ( $phone_Id ) != 32)
	die ( INVALID_PHONE_ID );
if (! (($avatar >= 'a' && $avatar <= 'z') || ($avatar >= 'A' && $avatar <= 'Z')))
	die ( WRONG_AVATAR );
	// search for username of phone_Id in database
$search_result = $db->select_query ( 'SELECT * FROM ' . TABLE_PREFIX . 'users WHERE username = ? OR phone_Id = ?;', array (
		$username,
		$phone_Id 
) );

if ($search_result) {
	if ($search_result [0] ['username'] == $username)
		die ( USER_EXISTS );
	if ($search_result [0] ['phone_Id'] == $phone_Id)
		die ( PHONE_ID_EXISTS );
}

$db->action_query ( "INSERT INTO `" . TABLE_PREFIX . "users`(`username`, `phone_Id`, `score`, 'avatar') VALUES (?,?,0,?)", array (
		$username,
		$phone_Id,
		$avatar 
) );
echo DONE;