-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2016 at 11:09 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zash`
--

-- --------------------------------------------------------

--
-- Table structure for table `boards`
--

CREATE TABLE `boards` (
  `game_Id` int(11) NOT NULL,
  `cards` text NOT NULL,
  `turn` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `game_Id` int(11) NOT NULL,
  `users` text NOT NULL,
  `winner` int(11) NOT NULL,
  `date` int(16) NOT NULL,
  `status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `moves`
--

CREATE TABLE `moves` (
  `move_Id` int(11) NOT NULL,
  `game_Id` int(11) NOT NULL,
  `player_Id` int(11) NOT NULL,
  `move` varchar(16) NOT NULL,
  `map` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `user_Id` int(11) NOT NULL,
  `game_Id` int(11) NOT NULL,
  `cards` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `phone_Id` varchar(32) CHARACTER SET utf32 COLLATE utf32_persian_ci NOT NULL,
  `score` int(10) NOT NULL,
  `avatar` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `phone_Id`, `score`, `avatar`) VALUES
('ali_asqar', '1029342756019948dj45019233746523', 0, 'T'),
('alireza', '10293847560192837465019283746523', 50, 'T'),
('amirali', '10293427560192337245019233746523', 42, 'T'),
('esmail', '1029j59f560192337245019233746523', 92, 'T'),
('fateme', '1029j5fdr60192337245019233746523', 25, 'T'),
('mohammadreza', '10293857560192337245019233746523', 25, 'T'),
('moin32', '102jtf97560192837465019283746523', 5, 'T'),
('nazanin', '1029j59f560192337m4jci9233746523', 12, 'T'),
('reza_exp96', '1029j5f48dj192337245019233746523', 80, 'T'),
('saba-574', '10293427chdu3948dj45019233746523', 23, 'T'),
('sara_1212', '1029jru4830192337245019233746523', 42, 'T'),
('sayid-84', '10293857560u58d37245019233746523', 1, 'T'),
('shahab', '102934275utjf2337245019233746523', 26, 'T'),
('shahin', '1029384756019948dj65019283746523', 56, 'T'),
('somaye', '1029j59f56mfjv337245019233746523', 72, 'T');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boards`
--
ALTER TABLE `boards`
  ADD PRIMARY KEY (`game_Id`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`game_Id`);

--
-- Indexes for table `moves`
--
ALTER TABLE `moves`
  ADD PRIMARY KEY (`move_Id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`user_Id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boards`
--
ALTER TABLE `boards`
  MODIFY `game_Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `game_Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `moves`
--
ALTER TABLE `moves`
  MODIFY `move_Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `user_Id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
