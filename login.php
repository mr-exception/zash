<?php
/*
 * login a user. check if it's registered
 */

// all statics
include_once 'includes/config.php';
include_once 'includes/database_handler.php';
$db = new database_handler ();

define ( 'DONE', 'D0' );
define ( 'WRONG_LOGIN', 'E1' );

$username = $_GET ['username'];
$phone_Id = $_GET ['phone_Id'];

// search for username of phone_Id in database
$search_result = $db->select_query ( 'SELECT * FROM ' . TABLE_PREFIX . 'users WHERE username = ? AND phone_Id = ?;', array (
		$username,
		$phone_Id 
) );

if ($search_result) {
	echo DONE;
} else {
	echo WRONG_LOGIN;
}
